
//////////////////////////////////////////////////////////////////////////////
// Funci�n SetQuantile y relacionadas

//////////////////////////////////////////////////////////////////////////////
// Se implementan los distintos m�todos para estimar los cuantiles de una 
// muestra siguiendo la documentaci�n de:
//   http://en.wikipedia.org/wiki/Quantile
// Corresponde con m�todos implementados en R, SAS y Excel.
// Los tres primeros (1-3) para muestras de una distribuci�n discreta, 
// los restantes (4-10) para las de una distribuci�n continua.
// Methods:
// (1) R-1, SAS-3
// (2) R-2, SAS-5
// (3) R-3, SAS-2
// (4) R-4, SAS-1
// (5) R-5
// (6) R-6, SAS-4, TOL
// (7) R-7, Excel
// (8) R-8
// (9) R-9
// (10) �?
// V�ase tambi�n "Sample Quantiles" en:
//   http://stat.ethz.ch/R-manual/R-devel/library/stats/html/quantile.html
//
// TOL dispone de una funci�n para la estimaci�n de los cuantiles
// de una muestra de n�mero reales: 
//   Real SetQuantile(SetOfReal set, Real probability)
// A la que asociamos el m�todo 0 y que coincide con el m�todo 6 (R-6, SAS-4)

//////////////////////////////////////////////////////////////////////////////
Real _SetQuantile.Method__h(Real method, Real N, Real p)
//////////////////////////////////////////////////////////////////////////////
{
  Case(method<:[[1,2,5]], N*p+1/2,
    method<:[[3,4]], N*p,
    method==6, (N+1)*p,
    method==7, (N-1)*p+1,
    method==8, (N+1/3)*p+1/3,
    method==9, (N+1/4)*p+3/8,
    method==10, (N+2)*p-1/2
  )
};

//////////////////////////////////////////////////////////////////////////////
Real _SetQuantile.Method__min(Real method, Real N, Real p)
//////////////////////////////////////////////////////////////////////////////
{
  Case(method==1, p <= 0,
    method==2, p <= 0,
    method==3, p <= (1/2)/N,
    method==4, p < 1/N,
    method==5, p < (1/2)/N,
    method==6, p < 1/(N+1),
    method==7, False,
    method==8, p < (2/3)/(N+1/3),
    method==9, p < (5/8)/(N+1/4),
    method==10, p < (3/2)/(N+2)
  )
};

//////////////////////////////////////////////////////////////////////////////
Real _SetQuantile.Method__max(Real method, Real N, Real p)
//////////////////////////////////////////////////////////////////////////////
{
  Case(method==1, p > 1,
    method==2, p >= 1,
    method==3, p > 1,
    method==4, p >= 1,
    method==5, p >= (N - 1/2)/N, 
    method==6, p >= N/(N+1),
    method==7, p >= 1,
    method==8, p >= (N-1/3)/(N+1/3),
    method==9, p >= (N-3/8)/(N+1/4),
    method==10, p >= (N+1/2)/(N+2)
  )
};

//////////////////////////////////////////////////////////////////////////////
Real SetQuantile.Method_Real(Set set, Real p, Real method)
//////////////////////////////////////////////////////////////////////////////
{
  If(method==0, {
    SetQuantile(set, p)
  }, {
    Set setS = Sort(set, Compare);
    Real N = Card(setS);
    Real h = _SetQuantile.Method__h(method, N, p);
    Case(_SetQuantile.Method__min(method, N, p), {
      setS[1]
    }, _SetQuantile.Method__max(method, N, p), {
      setS[N]
    }, True, {
      Case(method==1, {
        Real ChmM = Ceiling(h-1/2);
        setS[ChmM]
      }, method==2, {
        Real ChmM = Ceiling(h-1/2);
        Real FhpM = Floor(h+1/2);
        (setS[ChmM] + setS[FhpM])/2
      }, method==3, {
        Real Rh = Round(h);
        setS[Rh]
      }, method>=4, {
        Real Fh = Floor(h);
        setS[Fh] + (h-Fh) * (setS[Fh+1]-setS[Fh])
      })
    })
  })
};

//////////////////////////////////////////////////////////////////////////////
// Extensi�n de SetQuantile para otras gram�ticas

//////////////////////////////////////////////////////////////////////////////
Serie SetQuantile.Method_Serie(Set set, Real p, Real method)
//////////////////////////////////////////////////////////////////////////////
{
  Matrix m = Group("ConcatRows", EvalSet(set, SerMat));
  Matrix r = SetRow(For(1, Columns(m), Real (Real col) {
    Matrix column = SubCol(m, [[col]]);
    Set setS = Sort(MatSet(Tra(column))[1], Compare);
    SetQuantile.Method_Real(setS, p, method)
  }));
  MatSerSet(r, Dating(set[1]), First(set[1]))[1]
};

//////////////////////////////////////////////////////////////////////////////
Matrix SetQuantile.Method_Matrix(Set set, Real p, Real method)
//////////////////////////////////////////////////////////////////////////////
{
  Set sset = EvalSet(Traspose(EvalSet(set, MatSet)), Traspose);
  SetMat(For(1, Card(sset), Set (Real i) {
    For(1, Card(sset[i]), Real (Real j) {
      Set setS = Sort(sset[i][j], Compare);
      SetQuantile.Method_Real(setS, p, method)
    })
  }))
}; 

//////////////////////////////////////////////////////////////////////////////
Anything SetQuantile.Method.Grammar(Set set, Real p, Real method, 
  Text grammar)
//////////////////////////////////////////////////////////////////////////////
{
  Case(grammar=="Real", SetQuantile.Method_Real(set, p, method),
    grammar=="Serie", SetQuantile.Method_Serie(set, p, method),
    grammar=="Matrix", SetQuantile.Method_Matrix(set, p, method),
    True, {
      WriteLn("[SetQuantile.Method.Grammar] Gram�tica '"<<grammar
        <<"' no implementada", "E");
      If(False, ?)
  })
};

//////////////////////////////////////////////////////////////////////////////
Anything SetQuantile.Grammar(Set set, Real p, Text grammar)
//////////////////////////////////////////////////////////////////////////////
{
  SetQuantile.Method.Grammar(set, p, 0, grammar)
};
//////////////////////////////////////////////////////////////////////////////
